#!/usr/bin/python3
# coding=utf-8
from flask import Flask, session, request, redirect, url_for, render_template
from pyzotero import zotero
import os
import json


DEV_MOD = False


notes = {}
items = []
collections = []
documentaires = []
tags = []
token = '8RIabee55R3hHoPqmrIK37T1'
type_group = 'group'
id_group = '4873274'
zot = zotero.Zotero(id_group, type_group, token)


def writeJson(name, data):
    with open('data-base/'+name+".json", "w") as outfile:
        json.dump(data, outfile)

def loadJson(file):
    f = open('data-base/'+file+'.json')
    data = json.load(f)
    f.close()
    return data

def loadItems():
    # print('loaditems')
    zot = zotero.Zotero(id_group, type_group, token)
    count_items = zot.num_items()
    # print(count_items)

    x = 0
    items = []
    while x < count_items:
        its = zot.top(start=x)
        for it in its:
            items.append(it)
        x = x + 100
        print(x)

    data_collections = zot.collections()
    cols = []
    for dc in data_collections:
        cols.append({dc['data']['key'] : dc['data']['name']})

    return items, cols

def loadNotes():
    for col in collections:
        currentCol = zot.collection_items(list(col.keys() )[0])
        for myCol in currentCol :
            if myCol['data']['itemType'] == 'note':
                keyParent = myCol['data']['parentItem']
                noteAssocie = myCol['data']['note']
                notes.update({keyParent: noteAssocie})

    return notes;

def getIndexDoc(id_item):
    for x, item in enumerate(documents):
        if item['key'] == id_item: return x

def getIndexCol(id_item):
    for item in collections:
        k = [ k for k in item.keys()]
        v = [ v for v in item.values()]
        if k[0] == id_item:
            return v[0]

def updateNotes():
    try:
        for k, v in notes.items():
            ps = getIndexDoc(k)
            documents[ps].update({'note': v})
    except:
        pass

def updateCollections():
    try:
        for doc in documents:
            cols = []
            for id_col in doc['data']['collections']:
                v = getIndexCol(id_col)
                cols.append(v)
            doc.update({'collections_title': cols})
    except:
        pass

if DEV_MOD == True:
    documents = loadJson('documents')
    collections = loadJson('collections')
    notes = loadJson('notes')
else:
    documents, collections = loadItems()
    notes = loadNotes();
    writeJson('documents', documents)
    writeJson('collections', collections)
    writeJson('notes', notes)

updateNotes()
updateCollections()




def getDoc(cats):
    ds = []
    for doc in documents:
        for cat in cats:
            # print('cats -_>', cats,'cat --> ', cat)
            if cat in doc['data']['collections']:
                ds.append(doc)
    return ds

app = Flask(__name__, static_url_path='/static')

@app.route('/reload')
def reload():
    try:
        global documents
        global collections
        global notes
        documents, collections = loadItems()
        notes = loadNotes();
        writeJson('documents', documents)
        writeJson('collections', collections)
        writeJson('notes', notes)
        updateNotes()
        updateCollections()
        result = 0
    except:
        result = 1
        documents = []
        collections = []
        notes = []

    result = 0

    return render_template('reload.html', documents=documents, collections=collections, result=result)

@app.route('/')
@app.route('/<cat>')
@app.route('/<cat>/<subcat>')
def index(cat='', subcat=''):

    page = 'ressources'
    texte = ''
    direction = ''
    ebdTexte = ''

    if cat == '1234':
        ds = [];
        cs = collections
        texte = txt + about
        page = 'about'

    elif cat == 'ebd':
        ds = documents
        ebdTexte = txt
        cs = []
        direction = cat+'/'
        for col in collections:
            key, value  = list(col.items())[0]
            if 'EbD2022:' in value: cs.append(col)

        keys = [list(v.keys())[0] for v in cs]
        if subcat == '':
            ds = getDoc(keys)
        else:
            ds = getDoc([subcat])
    elif cat != '':
        cs = collections
        ds = getDoc([cat])
    else:
        cs = collections
        ds = documents

    return render_template('index.html', direction=direction, documents=ds, collections=cs, selection=cat, about=texte, page=page, ebdTexte=ebdTexte)

@app.route('/info/<element>')
def info(element = ''):
    for doc in documents:
        if element == doc['key']:
            try:
                return render_template('information.html', doc=doc)
            except:
                return '<3<3<3<3<3<3<3'




@app.route('/imprimer/<element>')
def imprimer(element = ''):
    counter = 0;
    myDocs = []

    for doc in documents:
        if element == doc['key']:
            try:
                return render_template('imprimer.html', doc=doc, cols=collections, documents=documents)
            except:
                return '<3<3<3<3<3<3<3'

@app.route('/printcat')
def printcat():
    cs = collections
    ds = documents

    return render_template('printcat.html', documents=documents, collections=cs)


app.run(debug=True)


app.run(debug=True)
